# cge-watch

A node.js webserver providing students of Cusanus-Gymnasium Erkelenz the possibility to stay updated whenever announcements are presented on the school's website. Recognized updates on announcements are send to the registrated students via email.