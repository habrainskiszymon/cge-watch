import getLogger from './logger';
export default (label, parseArgs, onlyFile) => {
  return getLogger(label, parseArgs, onlyFile);
};
