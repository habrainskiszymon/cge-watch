/* global process */
require('./../load_config.js');
// TODO: reduce redundant info when logging into file like [info]...
const winston = require('winston');
import './cycle';
require('winston-daily-rotate-file');
import path from 'path';
// TODO: Use path.join to ensure compatible paths

const inTestMode = (process.env.JEST_WORKER_ID !== undefined);

const enumerateErrorFormat = winston.format((info) => {
  if (info instanceof Error) {
    return Object.assign({
      message: info.message,
      stack: info.stack,
    }, info);
  }

  if(info.message instanceof Error) {
    info.message = info.message.message;
    info.stack = info.message.stack;
  }

  return info;
});

export default function getLogger(label, parseArgs=true, onlyFile=false) {
  // prevent db to log verbose JSON objects

  const customFormat = winston.format.combine(
      winston.format.timestamp(),
      winston.format.align(),
      enumerateErrorFormat(),
      winston.format.printf((info) => {
        const {
          timestamp, level, message, label, ...args
        } = info;

        const ts = timestamp.slice(0, 19).replace('T', ' ');
        const test = inTestMode ? '[test]' : '';
        return `${test} [${label}] [${level}] @ ${ts}: ${message} ${parseArgs ?
            Object.keys(args).length ?
                JSON.stringify(JSON.decycle(args), null, 2) :
                '' :
            ''}`;
      }),
  );

  let logDir = path.join(__dirname, '..', 'logs', inTestMode? 'test' : '');

  const infoTransport = new (winston.transports.DailyRotateFile)({
    filename: path.join(logDir, 'info', 'info-%DATE%.log'),
    datePattern: 'HH-DD-MM-YYYY',
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d',
  });

  const errorTransport = new (winston.transports.DailyRotateFile)({
    filename: path.join(logDir, 'error', 'error-%DATE%.log'),
    datePattern: 'HH-DD-MM-YYYY',
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d',
    level: 'error',
  });

  const logger = winston.createLogger(
      {
        level: 'info',
        format: customFormat,
        transports: [infoTransport, errorTransport],
        exitOnError: false,
      },
  );

  if (!onlyFile && process.env.NODE_ENV !== 'production') {
    logger.add(
        new winston.transports.Console(
            {
              format: winston.format.combine(
                  winston.format.colorize(),
                  customFormat,
              ),
              level: 'debug',
            },
        ),
    );
  }

  logger.stream = {
    write: function(message) {
      logger.info(message);
    },
  };

  if (label !== null) {
    logger.format = winston.format.combine(
        winston.format.label({label: label}),
        logger.format,
    );
  }

  return logger;
}
