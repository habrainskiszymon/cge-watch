import {Announcement, Teacher, Course} from '../models/';
import Helper from './../util/Helper'; // TODO: standardize imports
import cheerio from 'cheerio';
import getLogger from './../logging/';

const logger = getLogger('announcement-parser');

/** @constant */
const tableIdentifier = '.schuelervplan';
// import Helper = require('../util/Helper');
// const cheerio = require('cheerio');
/**
 * Class Parser.
 */
export default class Parser {
  /**
   * Parses
   * @param {string} html
   * @return {Array}
   */
  static parseAnnouncementDates(html) {
    const $ = cheerio.load(html);
    const dates = [];
    $('select[name=schdatumauswahl]').children('option').each((i, option) => {
      // convert from DD.MM.YYYY into YYYY-MM-DD (ISO 8601)
      dates.push(Helper.convertCgeDateIntoDate($(option).html()));
    });
    return dates;
  }

  /**
   * Parses and builds {@link Announcement} objects out of `html`.
   * @param {string} html
   * @param {Date} date
   * @return {Announcement[]}
   */
  static parseAnnouncements(html, date) {
    if (typeof html !== 'string' || html == null) {
      throw new TypeError('`html` must be a string and not null.');
    }
    if (html.length === 0) return [];
    if (!(date instanceof Date) || date == null) {
      throw new TypeError('`date` must be an instance of Date and not null.');
    }

    const $ = cheerio.load(html);
    // chunk the array and convert everything into strings
    let lastInterval = [1, 1];
    return Helper.chunkArray(
        $(`${tableIdentifier} > tbody > tr > td`).slice(7, -1), 6).
        map((cells) => {
          cells = cells.map((cell) => $(cell).text());
          try {
            cells[0] = cells[0].replace(/ /g, '');
            const teacher = new Teacher(cells[2] ?
                cells[2] :
                cells[5] ? cells[5] : null, null);
            const course = Course.parseAndCombineToCourse(cells[0], teacher);
            let periodInterval = Course.parsePeriodInterval(cells[1]);
            if (periodInterval == null) {
              periodInterval = lastInterval;
            } else {
              lastInterval = periodInterval;
            }
            const announcement = Announcement.createAnnouncement(course, date,
                cells[4], periodInterval);
            return announcement;
          } catch (e) {
            logger.error({
              msg: 'Could not parse announcement.',
              announcementInfo: cells,
              error: e,
            });

            throw e;
          }
        });
  }
}
