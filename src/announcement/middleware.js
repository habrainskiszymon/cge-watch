const rp = require('request-promise');
const logger = require('../logging')('announcement');
const Helper = require('../util/Helper');
const Parser = require('./Parser');
const urlAuthentication = require('../middleware/auth').middleware.urlAuthentication;
const fetchSchedule = require('../schedule');
const Course = require('../models/Course');

function fetchDropOffDates(req, res, next) {
  logger.info('Fetching announcement dates...');
  const sessionId = req.cgeSessionId;

  return rp(Helper.prepareOptionWithSessionId({
    method: 'GET',
    uri: process.env.CGE_URI + '/index.php?parentid=2&menuid=235&l0=8&l1=14',
    transform: body => {
      return Parser.parseAnnouncementDates(body);
    },
  }, sessionId)).then(
      dropOffDates => {
        req.cgeDropOffDates = dropOffDates;
        next();
        return 1;
      },
  ).catch(e => logger.error(e));
}

async function fetchAllDropOffs(req, res, next) {
  req.cgeDropOffEntries = [];
  for (let date of req.cgeDropOffDates) {
    let dropOffs = await fetchDropOffs(req,
        Helper.convertDateIntoCgeDate(date));

    req.cgeDropOffEntries.push(
        {
          dropOffs: dropOffs,
          date: formattedDate,
        });
  }
  next();
}

function fetchDropOffs(req, date) {
  logger.info('Fetching dropoffs...');
  const sessionId = req.cgeSessionId;

  return rp(Helper.prepareOptionWithSessionId({
    method: 'POST',
    uri: process.env.CGE_URI + '/index.php?parentid=2&menuid=235&l0=8&l1=14',
    transform: body => {
      return Parser.parseAnnouncements(body);
    },
    form: {
      schdatumauswahl: date,
    },
  }, sessionId)).then(dropOffs => {
    return dropOffs;
  }).catch(e => console.error(e));
}

function filterUser(req, res, next) {
  let i = 0;
  for (let dropOffEntry of req.cgeDropOffEntries) {
    dropOffEntry.dropOffs = dropOffEntry.dropOffs.filter(
        dropOff => req.cgeSchedule.hasCourse(extractCourse(dropOff)));

    dropOffEntry.dropOffs.sort((dropOffA, dropOffB) => {
      //sort against period
      dropOffA = Parser.standardizePeriodInterval(dropOffA.period);
      dropOffB = Parser.standardizePeriodInterval(dropOffB.period);
      if (dropOffA[0] > dropOffB[0]) {
        return 1;
      } else if (dropOffA[0] < dropOffB[0]) {
        return -1;
      } else if (dropOffA[1] > dropOffB[1]) {
        return 1;
      } else if (dropOffA[1] < dropOffB[1]) {
        return -1;
      }
      return 0;
    });

    req.cgeDropOffEntries[i] = dropOffEntry.dropOffs.length === 0 ?
        null :
        dropOffEntry;
    i++;
  }
  next();
}

function extractCourse(dropOff) {
  return new Course(dropOff.course.replace(/ /g, '').
      match(/(?<=Q(1|2)[a-zA-Z]{1,3})(LK|GK|G|L)\d{1,2}/g)[0], dropOff._subject,
      null, null); //in this case the teacher is not relevant
}

export const middleware = [
  urlAuthentication,
  fetchDropOffDates,
  fetchAllDropOffs,
  fetchSchedule,
  filterUser];
