import {
  Announcement,
  SwitchAnnouncement,
  ExamAnnouncement,
  ReexamAnnouncement,
  TaskAnnouncement,
  TaskAttendanceAnnouncement,
  UnknownAnnouncement,
  CancellationAnnouncement,
  Room,
  Teacher,
  Subject
} from './../models/';

export function beautifyAnnouncement(announcement) {
	if(!(announcement instanceof Announcement)) throw new TypeError('`announcement` must be an instance of a subclass of models/Announcement');
	if (announcement instanceof SwitchAnnouncement) {
		return '<span>' + beautifyRoom(announcement.from) + '</span>&nbsp;&#x27f6;&nbsp;<span>' + beautifyRoom(announcement.to) + '</span>';
	} else if (announcement instanceof ExamAnnouncement) {
		return `Course has been cancelled because of exam.`;
	} else if (announcement instanceof ReexamAnnouncement) {
		return 'Reexamination which takes place in ' + beautifyRoom(announcement.in);
	} else if (announcement instanceof CancellationAnnouncement) {
		return 'Course has been cancelled (Ausfall)';
	} else if (announcement instanceof TaskAnnouncement) {
		return 'A task will be left behind. (' + announcement.statement + '). Attendance is not required.';
	} else if (announcement instanceof TaskAttendanceAnnouncement) {
		return 'A task will be left behind. (' + announcement.statement + '). Attendance is required.';
	} else if (announcement instanceof UnknownAnnouncement) {
		return announcement.statement + '<i>This information has been directly extracted</i>';
	}
}
function makeBadge(html, badgeClass) {
    return `<span class="badge badge-${badgeClass}">` + html + '</span>';
}

export function beautifyType(type) {
	return {
		'reexam' : makeBadge('Reexamination', 'info'),
		'task' : makeBadge('Task', 'warning'),
		'exam' : makeBadge('Examination', 'success'),
		'taskattendance' : makeBadge('Task With Attendance', 'warning'),
		'switch' : makeBadge('Room Switch', 'info'),
		'cancellation' : makeBadge('Cancellation','success'),
		'unknown': makeBadge('Unknown Announcement', 'danger')
	}[type];
}

export function beautifyRoom(room) {
	if((room instanceof Room)) {
		return `${room.building}${room.roomNumber}`;
	} else {
		throw new TypeError('Can only beautify rooms.');
	}
}

export function beautifyPeriod(period) {
	if(period[0] === period[1]) {
		return period[0] + ".";
	} else {
		return period[0] + "." + "&nbsp;&#8212;&nbsp;" + period[1] + ".";
	}
}

export function beautifyCourseIdentifier(course) {
	let identifier = course.phase + "&#8212;" +  course.subject + "&#8212;" + course.type + "&#8212;" + course.number;
	if(course.isRepresentingPhase()) {
		identifier = course.phase;
	}
	return identifier;
}

export function beautifySubject(subjectContraction) {
    return Subject.getFullName(subjectContraction);
}

export function beautifyTeacher(teacher) {
	if(teacher == null || teacher instanceof Teacher) {
		return teacher? teacher.contraction : '-'
	} else {
		throw new TypeError('`teacher` must be either null or an instance of models/Teacher');
	}
}

export function beautifyDate(date) {
	return  date.toLocaleDateString('de-DE');
}
