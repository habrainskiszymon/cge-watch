/* global process */

import rp from 'request-promise';
import parseSchedule from '../../models/schedule/parse_schedule';
import populateRequestWithSessionId
  from '../session/populate_request_with_session_id';

export default function fetchScheduleMiddleware(req, res, next) {
  return fetchSchedule(req.session_id, req.user)
  .then((schedule) => {
      res.schedule = schedule;
  })
  .catch((error) => {
      next(error);
  });
}

export function fetchSchedule(sessionid, user) {
    return rp(populateRequestWithSessionId({
      method: 'GET',
      uri: process.env.CGE_URI + '/index.php?parentid=2&menuid=254&l0=8&l1=13',
      transform: (body) => {
        return parseSchedule(body, user.phase);
      },
    }, sessionid))
    .then((schedule) => {
      return schedule;
    });
}
