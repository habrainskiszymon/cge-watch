import rp from 'request-promise';
const _ = require('lodash');
import {convertISODateIntoCgeDate} from '../../models/announcement/utils';
import populateRequestWithSessionId
  from '../session/populate_request_with_session_id';
import {parseAnnouncements, groupAnnouncementsByDate} from '../../models/announcement/utils';

export default function fetchAnnouncementsMiddleware(req, res, next) {
  return fetchAnnouncements(req.session_id, req.announcement_dates)
    .then((announcements) => {
      next();
    })
    .catch((error) => {
      next(error);
      return null;
    });
}

/** @private */
function _fetchAnnouncementsOnDate(sessionId, date) {
  return rp(populateRequestWithSessionId({
    method: 'POST',
    uri: process.env.CGE_URI + '/index.php?parentid=2&menuid=235&l0=8&l1=14',
    transform: (body) => {
      return parseAnnouncements(body, date);
    },
    form: {
      schdatumauswahl: convertISODateIntoCgeDate(date),
    },
  }, sessionId))
  .then((announcements) => {
    return announcements;
  });
}

export function fetchAnnouncements(sessionId, dates) {
    return Promise.all(dates.map((date) => {
        return _fetchAnnouncementsOnDate(sessionId, date);
    }))
    .then((announcements) => {
        return groupAnnouncementsByDate(_.flatten(announcements));
    });
}
