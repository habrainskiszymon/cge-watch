import populateRequestWithSessionId
from '../session/populate_request_with_session_id';
import {parseAnnouncementDates} from '../../models/announcement/utils';
require('./../../load_config');

import rp from 'request-promise';

export default function fetchAnnouncementDatesMiddleware(req, res, next) {
    return fetchAnnouncementDates(req.session_id)
    .then((dates) => {
        req.announcement_dates = dates;
        return next();
    })
    .catch((error) => {
        return next(error);
    });
}

export function fetchAnnouncementDates(sessionId) {
    return rp(populateRequestWithSessionId({
        method: 'GET',
        uri: process.env.CGE_URI + '/index.php?parentid=2&menuid=235&l0=8&l1=14',
        transform: (body) => {
            return parseAnnouncementDates(body);
        },
    }, sessionId));
}
