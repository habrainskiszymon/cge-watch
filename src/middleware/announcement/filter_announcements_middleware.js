export default function filterAnnouncementsMiddleware(req, res, next) {
  req.filtered_announcements = filterAnnouncements(req.announcements, req.user.schedule);
  next();
}

export function filterAnnouncements(announcements, schedule) {
    const filteredAnnouncements = {};
    let date;
    for(let d in announcements) {
        date = new Date(d);
        if(date != 'Invalid Date') { // for-in protection
            for(let announcement of announcements[date]) {
                if(schedule.isAffectedBy(announcement)) {
                    if(filteredAnnouncements[date] == null) {
                        filteredAnnouncements[date] = [];
                    }
                    filteredAnnouncements[date].push(announcement);
                }
            }
        }
    }

    return filteredAnnouncements;
}
