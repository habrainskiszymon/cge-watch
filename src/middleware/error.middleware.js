import getLogger from '../logging';

const logger = getLogger('error-middleware');

export default function errorMiddleware(err, req, res, next) {
  logger.error(err);
  if (res.headersSent) {
    return next(err);
  }
  res.status(500);
  const errorResponse = {
    message: 'Cannot process request.',
  };
  if (process.env.NODE_ENV !== 'production') {
    Object.assign(errorResponse, {
      error: err.message,
      stack: err.stack,
    });
  }
  res.json(errorResponse);
}