export default function logoutMiddleware(req, res, next) {
	req.logout();
	return next();
}