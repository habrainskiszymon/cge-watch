import loggerFactory from './../../logging';
const logger = loggerFactory('auth-protect-middleware');


export default function protectMiddleware(req, res, next) {
	if(req.isAuthenticated()) {
		if(req.params.id != null && parseInt(req.params.id) === req.user.id || req.user.isAdmin) { /** todo: maybe insecure. make it more explicit.*/
			logger.info(req.user + " has been authorized.");
			res.locals.user = req.user;
			return next();
		} else {
			return res.status(401).json({
				message: 'Authentication error. Client is not allowed to access this route'
			});
		}
	} else {
		return res.redirect('/login')
	}
}
