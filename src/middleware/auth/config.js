import passport from 'passport';
import passportLocal from 'passport-local';
import User from './../../models/User';
const LocalStrategy = passportLocal.Strategy;
import loggerFactory from './../../logging';
const logger = loggerFactory('auth-middleware');

export default function configPassport() {
  // called when the user is going to be stored in session
  passport.serializeUser((user, done) => {
    logger.info('Serialize');
    logger.info(user);
    done(null, user.id);
  });

  // called when a user should be extracted via identifier
  passport.deserializeUser((req, id, done) => {
    logger.info('Deserialize');
    logger.info(id);
    User.findByPk(id).
        then((user) => {req.res.locals.user = user; return done(null, user)}).
        catch((error) => done(error));
  });

  passport.use(new LocalStrategy({
    usernameField: 'username',    // define the parameter in req.body that passport can use as username and password
    passwordField: 'password',
    passReqToCallback: true,
}, (req, username, password, done) => {
    return User.findOne({where: {username: username}}).then((user) => {
      if (user == null) {
          return done(null, false, {message: 'Unknown user'})
      };
      if (user.validPassword(password)) {
        return done(null, user);
      } else {
        return done(null, false, {message: 'Wrong password'});
      }
    }).catch((error) => {
      return done(error);
    });
  }));
}
