import passport from 'passport';

export default function (failureHandler) {// config wrapper
	return (req, res, next) => {
		(passport.authenticate('local', (err, user, info) => {
	        if (err) { return next(err); }
	        if (!user) {
				return failureHandler(info, req, res, next);
	        }
	        req.login(user, (errLogin) => {
	            if (errLogin) {
	                return next(errLogin);
	            }
	            return next()
	        });
	   }))(req, res, next)
	}
}
