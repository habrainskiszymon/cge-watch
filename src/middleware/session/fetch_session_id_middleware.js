/* global process */

import rp from 'request-promise';

export default function fetchSessionIdMiddleware(req, res, next) {
  return fetchSessionId(req.user.username, req.user.password)
  .then((sessionId) => {
    req.session_id = sessionId;
    next();
  })
  .catch(next);
}

export function fetchSessionId(username, password) {
    return rp({
      method: 'POST',
      uri: process.env.CGE_URI + '/index.php?menuid=116&parentid=115',
      form: {
        username: username,
        password: password,
      },
      resolveWithFullResponse: true,
      secure: true,
      transform: (body, res) => {
        return res.headers['set-cookie'][0].split(';')[0].split('=')[1];
      },
  });
}
