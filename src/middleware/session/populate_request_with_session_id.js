import tough from 'tough-cookie';
import rp from 'request-promise';

export default function populateRequestWithSessionId(
    requestOptions, sessionId) {
  const c = new tough.Cookie({
    key: process.env.CGE_SESS_KEY,
    value: sessionId,
    domain: process.env.CGE_DOMAIN,
    rejectPublicSuffixes: false,
    secure: true,
    httpOnly: true,
  });

  const ckJ = rp.jar();
  ckJ._jar.rejectPublicSuffixes = false;
  ckJ.setCookie(c.toString(), process.env.CGE_URI, (e) => {
  });

  return Object.assign({jar: ckJ}, requestOptions);
}
