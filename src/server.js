/* global process */
require('./load_config.js');
import app from './app';
import loggerFactory from './logging';
import nodePortCheck from 'node-port-check';

const logger = loggerFactory('server');

const nextAvailable = nodePortCheck.nextAvailable;


app.set('port', process.env.PORT || 9000);

nextAvailable(app.get('port'), '0.0.0.0').then((nextAvailablePort) => {
  app.set('port', nextAvailablePort);
  restarter(null, errorHandler => {
    app.listen(app.get('port')).on('listening', () => {
      logger.info(`App started on port ${app.get('port')}`);
      logger.info(process.env.NODE_ENV + ' mode.');
    }).on('error', errorHandler);
  });
});

let attempts = process.env.MAX_RESTART_ATTEMPTS;

function restarter(error, starterFn) {
  if (attempts <= 0) {
    logger.error('Could not start the server. Giving up now...');
    process.exit(1);
  }
  attempts--;
  starterFn((error) => {
    return restarter(error, starterFn);
  });
}