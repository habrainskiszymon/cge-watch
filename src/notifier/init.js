import RegisteredAnnouncement from '../models/RegisteredAnnouncement';
import loggerFactory from '../logging/';
import User from '../models/User';
const _ = require('lodash');
import jobSchedule from 'node-schedule';
import {fetchSessionId} from '../middleware/session/fetch_session_id_middleware';
import {fetchAnnouncementDates} from '../middleware/announcement/fetch_announcement_dates_middleware';
import {fetchAnnouncements} from '../middleware/announcement/fetch_announcements_middleware';
import {filterAnnouncements} from '../middleware/announcement/filter_announcements_middleware';
import {fetchSchedule} from '../middleware/schedule/fetch_schedule_middleware';
import nodemailer from 'nodemailer';
import path from 'path';
import LRU from 'lru-cache';
import ejs from 'ejs';
import announcementTemplateFunctions from '../routes/announcement/announcement_template_functions';


const EmailTemplate = require('email-templates').EmailTemplate;
require('../load_config');

const logger = loggerFactory('notifier-service');
const errorHandler = (err) => {
    logger.error(err);
}

let notifierJob = null;
let includePath = null;

function getAnnouncementsOutOf(user) {
    return new Promise((resolve, reject) => {
        if(user == null) {
            return reject(new Error('No user given.'));
        }
        resolve(user);
    })
    .then((user) => {
        return fetchSessionId(user.username, user.password);
    })
    .then((sessionId) => {
        return fetchAnnouncementDates(sessionId)
        .then((dates) => {
            return [sessionId, dates];
        })
    })
    .then((sessionIdAndDates) => {
        return fetchAnnouncements(sessionIdAndDates[0], sessionIdAndDates[1]);
    })
    .catch((err) => {
        throw err;
    });
}

function getAnnouncements() {
    //TODO: Combine
    const aQ1 = User.findOne({
        where: {
            phase:  'Q1'
        }
    })
    .then((user) => {
        return getAnnouncementsOutOf(user);
    })
    .then((announcements) => {
        return announcements;
    })
    .catch((err) => {
        if (err.message === 'No user given.') {
            logger.warn('Could not find user for Q1.');
        } else {
            err.message = `Cannot fetch announcements for Q1: ${err.message}.`;
            logger.error(err);
        }
    })

    const aQ2 = User.findOne({
        where: {
            phase:  'Q2'
        }
    })
    .then((user) => {
        return getAnnouncementsOutOf(user);
    })
    .then((announcements) => {
        return announcements;
    })
    .catch((err) => {
        if (err.message === 'No user given.') {
            logger.warn('Could not find user for Q2.');
        } else {
            err.message = `Cannot fetch announcements for Q2: ${err.message}.`;
            logger.error(err);
        }
    })

    return Promise.all([aQ1, aQ2]);
}

function notify() {
    logger.info('Executing notification cron-job...');

    return getAnnouncements()
    .then((data) => {
        const announcementsQ1 = removeOldAnnouncements(data[0]);
        const announcementsQ2 = removeOldAnnouncements(data[1]);

        User.findAll({
            where: {
                subscribed: true
            }
        }).then((users) => {
            if (users == null) {
                logger.info('No user to notify.');
                return;
            }

            const promises = users.map((user) => {
                if (process.env.IGNORE_NOTIFICATION_TIME_PREFERENCES === 'true' || user.preferredNotificationHour === new Date().getHours()) {
                    return fetchSessionId(user.username, user.password)
                    .then((sessionId) => {
                        return getOwnAnnouncementsFrom(((user.phase == 'Q1') ? announcementsQ1 : announcementsQ2), sessionId, user);
                    })
                    .then((announcements) => {
                        return mail(user, announcements);
                    })
                } else {
                    logger.debug(`Ignored ${user.username} because he either wants to be notified later on or he has been already notified. user.preferredNotificationHour = ${user.preferredNotificationHour}.`);
                }
            });

            return Promise.all(promises)
            .catch((error) => {
                error.message = `Unable to finish notification process: ${error.message}.`;
                logger.error(error);
            });
        })
    })
    .then(() => {
        logger.info('Notification process finished. Next notification process at ' + notifierJob.nextInvocation() + '.');
    })
    .catch((err) => {
        err.message = `Notification failed: ${err.message}`;
        logger.error(err);
    });
}

function getOwnAnnouncementsFrom(allAnnouncementsFromPhase, sessionId, user) {
    return fetchSchedule(sessionId, user).then((schedule) => {
        return filterAnnouncements(allAnnouncementsFromPhase, schedule);
    })
    .catch((error) => {
        logger.error(error);
    })
}

function removeOldAnnouncements(announcements) {
    if(announcements == null) return null;
    let date;
    for (let dateStr in announcements) {
        date = new Date(dateStr);
        if (date != 'Invalid Date') { // for-in protection
            if (date < new Date()) {
                delete announcements[date];
            }
        }
    }

    return announcements;
}

function mail(user, announcements) {
    return new Promise((resolve, reject) => {
        ejs.renderFile(path.join(includePath, 'emails', 'announcements.ejs'),
        Object.assign({
            user: user,
            announcements: announcements,
        }, announcementTemplateFunctions, {
            beautifyType: function (type) {
                return {
        'reexam' : 'Reexamination',
        'task' : 'Task',
        'exam' : 'Examination',
        'taskattendance' : 'Task With Attendance',
        'switch' : 'Room Switch',
        'cancellation' : 'Cancellation',
        'unknown': 'Unknown Announcement'
    }[type];
            }
        }),
        {
            cache: path.join(includePath, 'emails', 'announcements.ejs')
        }, function(err, str){
            if(err) {
                logger.error('Unable to load email template.');
                deactivateJob();
                return reject(err);
            }

            const auth =  {
                user: process.env.MAIL_USER,
                pass: process.env.MAIL_PASSWORD,
                port: 465,
                secure: true
            };

            const mailOptions = {
                from: 'cge-watch <' + process.env.MAIL_USER +'>',
                to: 'szymonhabrainski@gmail.com',
                subject: 'Your Announcements!',
                text: 'No text representation available for this email.',
                html: str,
            };

            console.log(str); // compiled string

            const transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: auth
            });
            transporter.sendMail(mailOptions, (err, res) => {
                if (err) {
                    return console.log(err);
                    reject(err);
                } else {
                    console.log(JSON.stringify(res));
                    resolve('Finished');
                }
            });
        });
    });
}

function activateJob() {
    logger.info('Turning on notification job...');
    if (notifierJob !== null) return;
    notifierJob = jobSchedule.scheduleJob("*/20 * * * * *", notify);
}

function deactivateJob() {
    logger.info('Turning off notification job...');
    notifierJob.cancel();
    notifierJob = null;
}

export default function initializeNotifier(app) {
    const engine = app.get('views engine');
    if(engine !== 'ejs') {
        throw new Error('Incompatible view engine. Require ejs.');
    }
    includePath = app.get('views');
    ejs.cache = new LRU(100);
    activateJob();
}
