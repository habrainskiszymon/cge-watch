const express = require('express');

function init404Router(app){
    app.use((req, res) => {
        res.status(404).send('We are sorry but we cannot find your requested resource.');
    });
}

module.exports = init404Router;