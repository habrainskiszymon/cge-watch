import express from 'express';
import UserRouter from './user/UserRouter';
import AuthRouter from './auth/AuthRouter';
import ScheduleRouter from './schedule/ScheduleRouter';
import AnnouncmentRouter from './announcement/AnnouncementRouter';

const router = express.Router();

router.get('/*', function(req, res, next) {
    req.session.flash = [];
    next();
});

router.use('/user', UserRouter);
router.use('/', AuthRouter);
router.use('/schedule', ScheduleRouter);
router.use('/announcement', AnnouncmentRouter);

export default router;
