const User = require('../../models/User');
const express = require('express');
const passport = require('passport');
const Util = require('../../util').Util;
const logger = require('../../logging')('auth-router');
const sequelize = require('sequelize');
const UserService = require('../../services/UserService');

/*
    passport.authMiddleware := Look if you are authenticated. If not, redirect to /login
    passport.authenticate   := Look for credentials and authenticate yourself
 */

function initAuthRouter(app) {
    const authRouter = express.Router();
    authRouter.get('/', (req, res) => {
        res.redirect('/dashboard')
    });

    authRouter.get('/dashboard', passport.authMiddleware.redirectIfNotAuthenticated, (req, res) => {
        UserService.getScheduleFrom(req.user)
            .then(schedule => res.renderSite('Dashboard', 'dashboard.ejs', req.bundleFlashedMessages({
                schedule: schedule,
                username: req.user.username
            })))
            .catch(error => {
                logger.error('Cannot load schedule from user:' + error);
                req.flashErrMessage('Internal Server Error', 'Sorry but we are not able to fetch your schedule at the moment.');
                res.renderSite('Dashboard', 'dashboard.ejs', req.bundleFlashedMessages({username: req.user.username}));
            });
    });

    authRouter.route('/signup')
        .get((req, res) => {
            res.renderSite('Sign Up', 'signup', req.bundleFlashedMessages());
        })
        .post((req, res) => {
            //check if we got storage permission
            logger.info(`Storage permission: ${req.body.storagePermission}`);

            const user = User.build({
                username: req.body.username,
                email: req.body.email,
                password: req.body.password
            });

            user.save()
                .then(user => {
                    req.flashSuccessMessage('User Registration', 'Your user account was successfully created.');
                    res.redirect('/login');
                })
                .catch(errors => {
                    logger.error(errors);
                    req.flashErrMessage('User Registration', 'Internal server error or invalid credentials.');
                    res.redirect('/signup')
                });
        });

    authRouter.route('/login')
        .get(passport.authMiddleware.redirectIfAuthenticated, (req, res) => {
            res.renderSite('Login', 'login', req.bundleFlashedMessages(), res);
        })
        .post(passport.authenticate('local', {
            successRedirect: '/dashboard',
            failureRedirect: '/login',
            failureFlash: true
        }));

    authRouter.get('/logout', passport.authMiddleware.redirectIfNotAuthenticated, (req, res) => {
        req.logout();
        res.redirect('/');
    });

    app.use(authRouter);
}

module.exports = initAuthRouter;