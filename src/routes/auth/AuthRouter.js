import express from 'express';
import passport from 'passport';
import loggerFactory from './../../logging';
import loginMiddleware from './../../middleware/auth/login_middleware';
import protectMiddleware from './../../middleware/auth/protect_middleware';
import logoutMiddleware from './../../middleware/auth/logout_middleware';
import User from './../../models/User';
import {ValidationError} from 'sequelize';
const logger = loggerFactory('auth-router');
import flash from 'flash';

const authRouter = express.Router();

// authRouter.post('/login',(req, res) => {
//     (passport.authenticate('local')
// }, (err, req, res) => {
//     console.log("sdasdsad");
//     if(err || !req.user) {
//         return res.status(401).json({
//             msg: "Hi",
//             error: err
//         });
//     } else {
//         return res.status(200).json(req.user);
//     }
// });

authRouter.get('/', (req, res) => {
    res.redirect('/login');
});

authRouter.get('/login', (req, res, next) => {
    if(req.isAuthenticated()) {
        return res.redirect('/announcement/' + req.user.id);
    } else {
        res.render('site.ejs', {
            title : 'Login',
            contentTemplate : 'login.ejs',
        });
    }
});

authRouter.post('/login', loginMiddleware((info, req, res, next) => {
    req.flash('error', info.message);
    res.redirect('/login');
}), (req, res) => {
    req.flash('info', 'Successfully logged in.')
    res.redirect('/announcement/' + req.user.id);
});

authRouter.get('/signup', (req, res) => {
    res.render('site.ejs', {
        title: 'Sign Up',
        contentTemplate: 'signup.ejs',
        allowCreateAdminCheckbox: (req.isAuthenticated() && req.user.isAdmin === true)
    });
});

authRouter.post('/signup', (req, res, next) => {
    req.body.agreeWithStorage = (req.body.agreeWithStorage === 'on')? true : false;
    req.body.isAdmin = (req.body.isAdmin === 'on')? true : false;

    if(req.body.isAdmin === true){
        if(!req.user || req.user.isAdmin !== true) {
            req.flash('error', 'Cannot create admin without being logged in as an admin.');
            return res.redirect('/signup');
        }
    }
    if (!req.body.agreeWithStorage){
        req.flash('error', 'Please accept the storage condition below.');
        return res.redirect('/signup');
    }
    User.create({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        isAdmin: req.body.isAdmin
    }).then((user) => {
        req.flash('info', 'User creation was successful.');
        return res.redirect('/login');
    }).catch(e => {
        if(e instanceof ValidationError) {
            for(error of e.errors) {
                req.flash('error', error.path + ' causes ' + error.type);
            }
            return res.redirect('/signup');
        }
        return next(e);
    });
});

authRouter.get('/logout', logoutMiddleware, (req, res) => {
    res.redirect('/login');
});

export default authRouter;
