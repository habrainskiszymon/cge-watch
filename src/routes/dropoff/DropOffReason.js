class DropOffReason {
    constructor(keyword, reasonName, fnPopulate) {
        this.keyword = keyword;
        this.reasonName = reasonName || 'n/a';
        this.fnPopulate = fnPopulate || (dropOff => {});
    }

    populateIfKeywordMatch(dropOff, givenReason) {
        if (givenReason.search(this.keyword) === -1) {
            dropOff.reason = this.reasonName;
            this.fnPopulate(dropOff);
        }
    }
}