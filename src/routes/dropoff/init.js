const express = require('express');

function initDropOffRouter(app, urlPrefix) {
    const router = express.Router();

    router.get('/:u/:p', ...require('../../announcement').user, (req, res) => {
        res.json(req.cgeDropOffEntries)
    });

    app.use('/announcement', router);
}

module.exports = initDropOffRouter;
