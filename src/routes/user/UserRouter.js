import User from '../../models/User';
import express from 'express';
import { ValidationError } from 'sequelize';
import protectionMiddleware from './../../middleware/auth/protect_middleware'

const userRouter = express.Router();

userRouter.route('/')
.get(protectionMiddleware, (req, res, next) => {
  User.findAll().then((users) => {
    let usersList = [];

    users.forEach(user => {
      usersList.push(user.toJSON());
    });

    res.status(200).json(usersList);

  }).catch((error) => res.status(404).json(e));
});

userRouter.route('/:id')
.get(protectionMiddleware, (req, res, next) => {
  User.findOne({
    where: {
      id: req.params.id,
    },
  })
  .then(user => {
    if (user === null) {
      return res.status(400).json({
        message: 'Cannot find user.',
        id: req.params.id
      }).end();
    }
    return res.status(200).json(user.toJSON()).end();
  })
  .catch((e) => {
    return next(e);
  });
})
// first search for the user and then delete it
.delete(protectionMiddleware, (req, res, next) => {
  User.findByPk(req.params.id)
    .then((user) => {
      if(user == null) {
        return res.status(404).json({
          message: 'User does not exists',
          id: req.params.id
        });
      }
      // do not allow normal users to delete admin
      if(user.isAdmin && req.user.username !== process.env.SUPER_ADMIN_NAME) {
        return res.status(401).json({
          message: 'Cannot delete admin when client is not logged in as the super-admin',
        });
      }
      user.destroy().then(() => {
        res.status(200).json({
          message: 'User has been successfully deleted.',
          user: user
        });
      }).catch((e) => {
        return next(e);
      });
    })
    .catch((e) => {return next(e)});
});

export default userRouter;
