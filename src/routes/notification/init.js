const User = require('../../models/User');
const logger = require('../../logging')('notification-router');
const express = require('express');
require('../../logging/cycle');

function defaultErrorHandler(error) {
    logger.error(error);
    throw error;
}

function getErrorResponse(error) {
    return {
        affectedUser: null,
        changed: false,
        error: true,
        message: error.message
    }
}

function initNotificationRouter(app) {
    const router = express.Router();
    require('../../notifier').init(app);
    router.get('/subscribe/:u', (req, res) => {
            req.createSubscriptionFor(req.params.u)
                .then(result => {
                    res.status(200).json(result);
                })
                .catch(defaultErrorHandler)
                .catch(error => res.status(500).json(getErrorResponse(error)))
        }
    );


    router.get('/unsubscribe/:u', (req, res) => {
            req.deleteSubscriptionFor(req.params.u)
                .then(result => {
                    res.status(200).json(result);
                })
                //.catch(defaultErrorHandler)
                .catch(error => res.status(500).json(getErrorResponse(error)));
        }
    );

    router.get('/subscribers/', (req, res) => {
        User.find({
            where: {
                subscribed: true
            }
        })
            .then(subscribers => res.status(200).json({
                    subscribers: subscribers,
                    nextNotification: req.getNextPlannedNotification()
                })
            )
            .catch(defaultErrorHandler);
    });

    router.get('/test/', (req, res) => {
        User.findByPk(1).then(user => {
            user.subscribed = false;
            user.save().then( user =>
                res.send(user.subscribed)
            )
        })
    });

    app.use('/', router);
}

module.exports = initNotificationRouter;