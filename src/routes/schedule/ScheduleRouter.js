import express from 'express';
import protectMiddleware from './../../middleware/auth/protect_middleware';
import fetchSessionIdMiddleware from './../../middleware/session/fetch_session_id_middleware';
import fetchScheduleMiddleware from './../../middleware/schedule/fetch_schedule_middleware';
const scheduleRouter = express.Router();

scheduleRouter.get('/:id',
	protectMiddleware,
 	fetchSessionIdMiddleware,
 	fetchScheduleMiddleware,
 	(req, res) => {
		return res.status(200).json({
			session: req.session_id,
			schedule: req.schedule.toJSON()
		});
});

export default scheduleRouter;
