import express from 'express';
import protectMiddleware from './../../middleware/auth/protect_middleware';
import fetchSessionIdMiddleware from './../../middleware/session/fetch_session_id_middleware';
import fetchScheduleMiddleware from './../../middleware/schedule/fetch_schedule_middleware';
import fetchAnnouncementDatesMiddleware from './../../middleware/announcement/fetch_announcement_dates_middleware';
import fetchAnnouncementsMiddleware from './../../middleware/announcement/fetch_announcements_middleware';
import filterAnnouncementsMiddleware from './../../middleware/announcement/filter_announcements_middleware';
import announcementTemplateFunctions from './announcement_template_functions';



const announcementRouter = express.Router();

announcementRouter.get('/:id',
	protectMiddleware,
	fetchSessionIdMiddleware,
	fetchScheduleMiddleware,
	fetchAnnouncementDatesMiddleware,
	fetchAnnouncementsMiddleware,
	filterAnnouncementsMiddleware,
	(req, res) => {
		return res.render('site.ejs', Object.assign({
				title: 'Announcements for ' + req.user.username,
				contentTemplate: 'announcements.ejs',
				announcements : req.announcements_filtered,
			}, announcementTemplateFunctions)
		);
	});

export default announcementRouter;
