import {
	beautifyAnnouncement,
	beautifyType,
	beautifyRoom,
	beautifyPeriod,
	beautifyCourseIdentifier,
	beautifyTeacher,
	beautifyDate,
	beautifySubject
} from '../../views/beautifier';

import moment from 'moment';

const fn = {
	moment: moment,
	beautifyAnnouncement : beautifyAnnouncement,
	beautifyType : beautifyType,
	beautifyRoom : beautifyRoom,
	beautifyPeriod : beautifyPeriod,
	beautifyCourseIdentifier: beautifyCourseIdentifier,
	beautifyTeacher: beautifyTeacher,
	beautifyDate: beautifyDate,
	beautifySubject: beautifySubject,
}

export default fn;
