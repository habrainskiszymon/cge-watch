/* global describe, require */

describe('__tests__ models', () => {
  require('./model/Teacher.test');
  require('./model/Subject.test');
  require('./model/Room.test');
  require('./model/Course.test');
  require('../models/schedule/__tests__/Schedule.unit.test');
  require('./model/Announcement.test');
});

describe('testing middleware', () => {
  require('./announcement/index.test');
});
