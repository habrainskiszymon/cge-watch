/* global describe, it, __tests__, expect */

import Course from '../../models/Course';
import Teacher from '../../models/Teacher';

describe('__tests__ models Course', () => {
  test('should instantiate', () => {
    const teacher = new Teacher('Td', null, null);
    const subject = 'M';
    const level = 2;
    const course = new Course('L', 2, subject, teacher);
    expect(course.level).toEqual('LK');
    expect(course.number).toEqual(level);
    expect(course.subject).toBe(subject);
    expect(course.teacher).toBe(teacher);
    expect(course.toJSON()).toEqual({
      level: 'LK',
      number: level,
      subject: subject,
      teacher: teacher.toString(),
    });
  });

  test('should instantiate with minimal information', () => {
    const teacher = new Teacher('Td', null, null);
    const subject = 'M';
    const level = 2;
    const course = new Course('L', 2, subject, teacher);
    expect(course.level).toEqual('LK');
    expect(course.number).toEqual(level);
    expect(course.subject).toBe(subject);
    expect(course.teacher).toBe(teacher);
  });

  test('should recognize equality', () => {
    const teacher = new Teacher('Td', null, null);
    const courseA = new Course('L', 3, 'M', teacher);
    const courseB = new Course('L', 3, 'M', teacher);
    expect(courseA.isEqualTo(courseB)).toBeTruthy();
    expect(courseB.isEqualTo(courseA)).toBeTruthy();
    const courseC = new Course('L', 2, 'M', teacher);
    expect(courseA.isEqualTo(courseC)).toBeFalsy();
    expect(courseC.isEqualTo(courseB)).toBeFalsy();
  });

  test('should parse interval string', () => {
    expect(Course.parsePeriodInterval('1./2.')).toEqual([1, 2]);
    expect(Course.parsePeriodInterval('5.')).toEqual([5, 5]);
    expect(Course.parsePeriodInterval('')).toBeNull();
  });

  test('should fail parsing interval string', () => {
    expect(() => {
      Course.parsePeriodInterval('hello/bug');
    }).toThrowError(new TypeError('Invalid `intervalString` given.'));

    expect(() => {
      Course.parsePeriodInterval('/1');
    }).toThrowError(new SyntaxError('Invalid `intervalString` given.'));

    expect(() => {
      Course.parsePeriodInterval('2/1');
    }).toThrowError(new SyntaxError('Invalid `intervalString` given.'));
  });

  it('should accept valid period intervals', () => {
    expect(Course.isValidPeriodInterval([1, 2])).toBeTruthy();
  });
});