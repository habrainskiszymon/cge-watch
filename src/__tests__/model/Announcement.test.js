/* global describe, __tests__, expect */

import Course from '../../models/Course';
import Teacher from '../../models/Teacher';
import {
  Announcement,
  ExamAnnouncement,
  SwitchAnnouncement,
} from '../../models/announcement/Announcement';
import Room from '../../models/Room';

describe('__tests__ models Announcement and its subclasses', () => {
  const courseA = new Course('L', 2, 'M',
      new Teacher('Td', Teacher.GENDER.MALE, 'Thaddäus'),
  );
  const date = new Date();

  test('should instantiate', () => {
    const switchAnnouncement = new SwitchAnnouncement(courseA, date, [1, 2],
        new Room('A', 102), new Room('B', 202));
    expect(switchAnnouncement.course).toBe(courseA);
    expect(switchAnnouncement.date).toBe(date);
  });

  test(
      'should throw error when trying to instantiate SwitchAnnouncement' +
      ' with two identical rooms',
      () => {
        expect(() => {
          new SwitchAnnouncement(courseA, date, [1, 2],
              new Room('A', 102), new Room('A', 102));
        }).
            toThrowError(
                new Error('Not real movement given.'),
            );
      });

  test(
      'should throw error when trying to instantiate abstract class Announcement',
      () => {
        expect(() => {
          new Announcement(courseA, date, [1, 2]);
        }).
            toThrowError(
                new Error('Cannot instantiate abstract class Announcement.'),
            );
      });

  test(
      'should factorize a SwitchAnnouncement' +
      ' and extract `_from` and `_to` of it',
      () => {
        const switchAnnouncement = Announcement.createAnnouncement(courseA,
            date,
            'CBG 1.22-&gt; A122', [1, 2]);
        expect(switchAnnouncement.from).toEqual(new Room('CBG', '122'));
        expect(switchAnnouncement.to).toEqual(new Room('A', '122'));
      });

  test('should factorize correct Announcement out of statement', () => {
    expect(Announcement.createAnnouncement(courseA, date,
        'Raumverschiebung: A102 -&gt; B202', [1, 2])).
        toBeInstanceOf(SwitchAnnouncement);
    expect(Announcement.createAnnouncement(courseA, date, 'Klausur', [1, 2])).
        toBeInstanceOf(ExamAnnouncement);
  });
});
