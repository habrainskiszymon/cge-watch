/* global describe, __tests__, expect */

import Room from '../../models/Room';

describe('__tests__ models Room', () => {
  test('should instantiate with normal CGE room notation', () => {
    const room = new Room('A', 108);
    expect(room.toJSON()).toEqual({
      building: 'A',
      roomNumber: 108,
    });
  });

  test('should parse CBG room string', () => {
    expect(Room.parseRoom('CBG 1.22')).toBeInstanceOf(Room);
    expect(Room.parseCBGRoom('CBG 1.22').toJSON()).toEqual({
      building: 'CBG',
      roomNumber: 122,
    });
  });

  test('should print human-readable string of room', () => {
    expect(Room.parseRoom('A101').toString()).toEqual('A101');
    expect(Room.parseRoom('CBG 1.22').toString()).toEqual('CBG122');
  });

  test('should throw error because of unknown' +
      ' or faulty building/room number', () => {
    expect(() => {
      new Room('Princess\' land', 666);
    }).toThrowError(new Error('Unknown building \'Princess\' land\'.'));

    expect(() => {
      new Room('A', -666);
    }).toThrowError(new RangeError('`roomNumber` cannot be negative.'));
  });

});