/* global describe, __tests__, expect */

import Teacher from '../../models/Teacher';

describe('__tests__ models Teacher', () => {
  test('should correctly instantiate a teacher', () => {
    const teacher = new Teacher('Td', Teacher.GENDER.MALE, 'Thaddäus');
    expect(teacher.gender).toEqual(Teacher.GENDER.MALE);
    expect(teacher.address).toEqual('Mr.');
    expect(teacher.name).toEqual('Thaddäus');
    expect(teacher.contraction).toEqual('Td');
  });

  test('should correctly stringify a teacher', () => {
    let teacher = new Teacher('Td', Teacher.GENDER.MALE, 'Thaddäus');
    expect(teacher.toString()).toEqual('Mr. Thaddäus (Td)');
    teacher = new Teacher('Sb', Teacher.GENDER.UNKNOWN, 'Spongebob');
    expect(teacher.toString()).toEqual('Spongebob (Sb)');
  });

  test('should instantiate teacher with minimal information', () => {
    const teacher = new Teacher('Td');
    expect(teacher.toString()).toEqual('(Td)');
  });
});