/* global describe, __tests__, expect */

import Subject from '../../models/Subject';

describe('__tests__ models Subject', () => {
  const unknownSubject = 'XYZ';
  test('should correctly determine whether it is' +
      ' a correct subject or not', () => {
    expect(Subject.isValidSubjectContraction(unknownSubject)).toBeFalsy();
    expect(Subject.isValidSubjectContraction('BI')).toBeTruthy();
    expect(Subject.isValidSubjectContraction('LT')).toBeTruthy();
  });

  test('should return right subject name', () => {
    expect(Subject.getSubject('M')).toEqual(['M', 'Mathematics']);
    expect(Subject.getSubject('GE')).toEqual(['GE', 'History Advanced']);
    expect(Subject.getSubject('GEB')).toEqual(['GEB', 'History Bilingual']);
    expect(Subject.getSubject(unknownSubject)).toEqual(null);
  });

  test('should return right subject full name', () => {
    expect(Subject.getFullName('D')).toEqual('German (D)');
    expect(Subject.getFullName('PL')).toEqual('Philosophy (PL)');
    expect(Subject.getFullName('SP')).toEqual('Physical Education (SP)');
    expect(Subject.getFullName(unknownSubject)).toEqual(null);
  });

});