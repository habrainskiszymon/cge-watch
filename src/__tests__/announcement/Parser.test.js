/* global , describe, it, expect */

import Parser from './../../announcement/Parser';
import {getHtmlProbe} from './../helper';

describe.each([
  getHtmlProbe('announcement', 'probe_08_01_2019.html'),
  getHtmlProbe('announcement', 'probe_09_01_2019.html'),
  getHtmlProbe('announcement', 'probe_10_01_2019.html')],
)(
    'use html probes (%# of 3)', (html) => {
      it('should parse announcements', () => {
        expect(Parser.parseAnnouncements(html, new Date('January 8, 2018')).
            map((a) => a.toJSON())).
            toMatchSnapshot();
      });
      it('should parse dates', () => {
        expect(Parser.parseAnnouncementDates(html)).toMatchSnapshot();
      });
    });


