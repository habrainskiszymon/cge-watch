/* global __dirname */

import fs from 'fs';
import path from 'path';

/**
 * Loads a html probe.
 * @param {string} category
 * @param {string} name
 * @return {string}
 */
export function getHtmlProbe(category, name) {
  const data = fs.readFileSync(
      path.join(__dirname, 'probes', category, name),
      'utf8');
  return data;
}
