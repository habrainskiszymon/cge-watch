class VisualMessage {
    constructor(type, title, content) {
        if (!(['error', 'success'].includes(type)))
            throw TypeError('Type must be error or success. Given: ' + type + '.');

        this.type = type;
        this.title = String(title);
        this.content = String(content);
    }
}

module.exports = VisualMessage;