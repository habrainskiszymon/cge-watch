import express from 'express';
import bodyParser from 'body-parser';
import expressSession from 'express-session';
import morgan from 'morgan';
import passport from 'passport';
import appRouter from './routes/AppRouter';
import loggerFactory from './logging';
import configPassport from './middleware/auth/config';
import errorMiddleware from './middleware/error.middleware';
import path from 'path';
import flash from 'flash';
import initializeNotifier from './notifier/init';
const logger = loggerFactory('core');

configPassport();

// invoke an instance of express application.
const app = express();

// initialize locals
app.use((req, res, next) => {
    res.locals.user = null;
    next();
});
// set ejs as our view engine
// it is needed to provide visual response after send in a form
app.set('views engine', 'ejs');
app.set('view cache', false);
app.set('views', path.join(__dirname, '/views'));
// set morgan to log info about our requests for development use
app.use(morgan('combined',
    {stream: (loggerFactory('http')).stream}));

// initialize body-parser to parse incoming parameters requests to req.body
app.use(bodyParser.urlencoded({extended: false}));

// setup passport
app.use(expressSession({
  secret: 'do not tamper',
  resave: true,
  saveUninitialized: true,
}));

app.use(passport.initialize());
app.use(passport.session());
app.use((req, res, next) => {
    if(req.user != null) {
        logger.info('Logged in user was saved in local variable.');
        res.locals.user = req.user;
    } else {
        logger.warn(`Guest has been detected [${req.connection.remoteAddress}].`);
    }
    next();
});
app.use(flash());

app.use(express.static(__dirname + '/public'));

app.use(appRouter);
app.use(errorMiddleware);

// start notifier
initializeNotifier(app);


if(process.env.IGNORE_NOTIFICATION_TIME_PREFERENCES == 'true') {
  logger.warn(
    'IGNORE_NOTIFICATION_TIME_PREFERENCES is set to true.' +
    ' This causes hourly notifications which will be sent to all users.' +
    ' The information given there will be redundant. Please consider it switching it off.'
    );
}

logger.info('App successfully configured.');

export default app;
