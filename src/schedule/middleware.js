const urlAuthentication = require('../middleware/auth').middleware.urlAuthentication;
const logger = require('../logging')('schedule');
const Helper = require('../util/Helper');
const rp = require('request-promise');
const Schedule = require('../models/schedule/Schedule');
const cheerio = require('cheerio');
const Course = require('../models/Course');

function fetchSchedule(req, res, next) {
  logger.info('SESSIONID: ' + req.cgeSessionId);
  return rp(Helper.prepareOptionWithSessionId({
    method: 'GET',
    uri: process.env.CGE_URI + '/index.php?parentid=2&menuid=254&l0=8&l1=13',
    transform: body => {
      return parseSchedule(body);
    },
  }, req.cgeSessionId)).then(schedule => {
    req.cgeSchedule = schedule;
    next();
  }).catch(error => logger.error(error));
}

function parseSchedule(body) {
  let schedule = new Schedule();
  const $ = cheerio.load(body);
  $('#Plan table tbody').
      children('tr').
      slice(2, 13).
      each((periodIdx, periodRow) => {
        $(periodRow).children('td').slice(2, 7).each((dayNo, courseTable) => {
          let courseInfo = $(courseTable).find($('tbody tr'));
          let courseSubjectAndId = $(courseInfo.first());
          let courseTeacherAndRoom = $(courseInfo.eq(1));

          let course = {};

          course.subject = courseSubjectAndId.children('td').first().text();

          if (course.subject === '') {
            course = null;
          } else {
            course.id = courseSubjectAndId.children('td').eq(1).text();
            course.teacher = courseTeacherAndRoom.children('td').first().text();
            course.room = courseTeacherAndRoom.children('td').eq(1).text();
          }
          course = course == null ?
              null :
              new Course(course.id, course.subject, course.teacher,
                  course.room);
          schedule.setCourseAppointment(dayNo, course);
        });
      });

  return schedule;
}

module.exports = [urlAuthentication, fetchSchedule];
