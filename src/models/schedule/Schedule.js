/**
 * @module model/Schedule
 */

import Course from '../Course';
import Room from '../Room';
import {Announcement} from '../announcement/Announcement';

export default class Schedule {
  /**
   * Hello
   * @constructor
   */
  constructor() {
    this._grid = [];
    for (let i = 0; i < 5; i++) {
      this._grid.push([]);
      for (let j = 0; j < 11; j++) {
        this._grid[i].push(null);
      }
    }
  }

  static checkAppointmentPointer(day, period) {
    if (day < 0 || 4 < day) {
      throw new RangeError('`day` must be between zero and 4');
    }
    if (period < 0 || 11 < period) {
      throw new RangeError('`day` must be between zero and 11');
    }
  }

  getCourseAppointment(day, period) {
    Schedule.checkAppointmentPointer(day, period);
    return this.grid[day][period];
  }

  setCourseAppointment(day, period, course, room) {
    Schedule.checkAppointmentPointer(day, period);

    if (course == null) {
      this.grid[day][period] = null;
      return;
    }

    if (course instanceof Course) {
      if (room instanceof Room && room != null) {
        this.grid[day][period] = [course, room];
      } else {
        throw new TypeError('`room` must be an instance of Room and not null.');
      }
    } else {
      throw new TypeError('`course` must be either of type Course or null.');
    }
  }

  static getDayName(dayIdx) {
    return Schedule.getDayNames()[dayIdx];
  }

  static getDayNames() {
    return ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'];
  }

  getCoursesOnDay(dayIdx) {
    return this.grid[dayIdx];
  }

  get grid() {
    return this._grid;
  }

  isAffectedBy(announcement) {
    if (announcement == null) return false;
    if (announcement instanceof Announcement) {
      return this.hasCourse(announcement.course);
    } else {
      throw new TypeError(
          `\`announcement\` must be an instance of Announcement or null.
           ${announcement.constructor.name} given.`);
    }
  }

  hasCourse(course) {
    if (course == null) return false;
    if (!(course instanceof Course)) {
      throw new TypeError('`course` must be an instance of Course.');
    }
    return this.grid.some((coursesOnDay) => {
      return coursesOnDay.some((courseInGrid) => {
        if (courseInGrid == null) return false;
        return courseInGrid[0].isEqualTo(course);
      });
    });
  }

  toJSON() {
    return this.grid.map((coursesOnDay) => coursesOnDay.map((courseInGrid) => {
      if (courseInGrid == null) return null;
      return [courseInGrid[0].toJSON(), courseInGrid[1].toJSON()];
    }));
  }
}
