/**
 * cge-watch
 *
 * @author paulowski
 * @module model/schedule
 * @version 1.0
 */

import cheerio from 'cheerio';
import Schedule from './Schedule';
import Course from '../Course';
import Teacher from '../Teacher';
import Room from '../Room';

/**
 * Specifies the identifier the table has to have in order to
 * locate the required data.
 * @const {String}
 * @default '#Plan'
 */
const TABLE_IDENTIFIER = '#Plan';

/**
 * Parses and instantiates a schedule from `html`
 * @param {string} html - HTML which provides a correctly structured table
 * inside a container with {@link TABLE_IDENTIFIER} as its identifier.
 * @param {string} phase - Specifies the phase. With that the courses will be
 * compiled.
 * @throws {TypeError|Error} Throws an error if html is not a string or if
 * parsing fails some wise.
 * @return {Schedule}
 */
export default function parseSchedule(html, phase = 'Q2') {
  if (typeof html !== 'string') {
    throw new TypeError('`html` must be a string.');
  }

  const $ = cheerio.load(html);
  const schedule = new Schedule();

  // lets imagine a table (as it actually is):
  // skip 12 (metadata) and jump every 7 items to get into a row (period).
  // Add day (day) to it to reach a certain course in a column which takes
  // place (or not).
  let course;
  let room;
  for (let day = 0; day < 5; day++) {
    for (let period = 0; period < 11; period++) {
      const COURSE_INFO = $(`${TABLE_IDENTIFIER} > table > tbody > tr > td`).
          eq(12 + 7 * period + day).
          find('td').
          map((i, e) => {
            return $(e).text();
          }).
          get();
      course = COURSE_INFO.length === 0 ?
          null :
          Course.parseAndCombineToCourse(
              phase + COURSE_INFO[0] + COURSE_INFO[1],
              new Teacher(COURSE_INFO[2]));

      room = Room.parseRoom(COURSE_INFO[3]);
      schedule.setCourseAppointment(day, period, course, room);
    }
  }
  // todo: schedule empty => null
  return schedule;
}
