/* global describe, it, expect */

import {
  getSampleCourses,
  getSampleRooms,
  getSampleSchedule,
} from 'sample_generator_functions';

describe('testing models/schedule/Schedule', () => {
  // Cannot avoid to integrate schedule with courses.
  // Nevertheless this is/these a(re) 'unit(s)' test suite(s)

  const {courseA, courseB, courseC, courseD} = getSampleCourses();
  const {roomA, roomB} = getSampleRooms();

  it('should instantiate and confirm having inserted courses', () => {
    const schedule = getSampleSchedule();

    expect(schedule.hasCourse(courseA)).toBeTruthy();
    expect(schedule.hasCourse(courseB)).toBeTruthy();
    expect(schedule.hasCourse(courseC)).toBeTruthy();
    expect(schedule.hasCourse(courseD)).toBeFalsy();
  });

  it('should output correct json representation', () => {
    const schedule = getSampleSchedule();

    expect(schedule.toJSON()).toEqual([
      [
        [courseA.toJSON(), roomA.toJSON()],
        null,
        [courseA.toJSON(), roomA.toJSON()],
        null,
        null,
        null,
        null,
        null,
        null,
        [courseB.toJSON(), roomB.toJSON()],
        [courseC.toJSON(), roomB.toJSON()],
      ],
      [
        null,
        null,
        null,
        [courseB.toJSON(), roomA.toJSON()],
        [courseC.toJSON(), roomB.toJSON()],
        null,
        null,
        null,
        null,
        null,
        null,
      ],
      [
        null,
        null,
        [courseB.toJSON(), roomA.toJSON()],
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
      ],
      [
        null, null, null, null, null, null, null, null, null, null, null],
      [
        [courseA.toJSON(), roomB.toJSON()],
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null],
    ]);
  });
});
