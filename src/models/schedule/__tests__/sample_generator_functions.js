import Schedule from '../Schedule';
import Course from '../../Course';
import Teacher from '../../Teacher';
import Room from '../../Room';

/**
 * Generates a schedule sample.
 * @return {Schedule}
 */
export function getSampleSchedule() {
  const [courseA, courseB, courseC] = getSampleCourses();
  const [roomA, roomB] = getSampleRooms();

  const schedule = new Schedule();
  schedule.setCourseAppointment(0, 0, courseA, roomA);
  schedule.setCourseAppointment(0, 2, courseA, roomA);
  schedule.setCourseAppointment(0, 9, courseB, roomB);
  schedule.setCourseAppointment(0, 10, courseC, roomB);
  schedule.setCourseAppointment(1, 3, courseB, roomA);
  schedule.setCourseAppointment(1, 4, courseC, roomB);
  schedule.setCourseAppointment(2, 2, courseB, roomA);
  schedule.setCourseAppointment(4, 0, courseA, roomB);
  return schedule;
}

export function getSampleRooms() {
  const roomA = new Room('A', 101);
  const roomB = new Room('TH');

  return [roomA, roomB];
}

export function getSampleCourses() {
  const courseA = new Course('Q2', 'M', 'LK', 2,
      new Teacher('Td', Teacher.GENDER.MALE, 'Thaddäus'),
  );
  const courseB = new Course('Q2', 'N', 'LK', 1,
      new Teacher('Ob', Teacher.GENDER.UNKNOWN, null),
  );
  const courseC = new Course('Q2', 'N', 'L', 1,
      new Teacher('Qs', Teacher.GENDER.FEMALE, 'Quos'),
  );
  const courseD = new Course('Q2', 'LT', 'G', 1,
      new Teacher('Ho', Teacher.GENDER.MALE, 'Hose'),
  );

  return [courseA, courseB, courseC, courseD];
}
