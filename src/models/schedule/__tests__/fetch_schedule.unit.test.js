/* global describe, it, expect */

import fetchSchedule from '../fetch_schedule';
import {getHtmlProbe} from '../../../__tests__/helper';

describe('testing models/schedule/parser_schedule', () => {
  it.each([
    getHtmlProbe('schedule',
        'probe_schedule.html')])('should parse schedule from probe (%#/0)',
      (html) => {
        expect(fetchSchedule(html).toJSON()).toMatchSnapshot();
      });
});
