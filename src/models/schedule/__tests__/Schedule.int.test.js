/* global describe, it, expect */

import Schedule from '../Schedule';
import Course from '../../Course';
import Teacher from '../../Teacher';
import Room from '../../Room';
import {
  getSampleCourses,
  getSampleRooms,
  getSampleSchedule,
} from './sample_generator_functions';

import {
  PermanentSwitch,
  ReexamAnnouncement,
  SwitchAnnouncement,
} from '../../announcement/Announcement';

describe('testing integration of models/schedule/Schedule', () => {
  it('should instantiate and confirm having inserted courses', () => {
    const schedule = getSampleSchedule();
    const [roomA, roomB] = getSampleRooms().slice(0, 2);
    const courseA = getSampleCourses()[0];
    console.log(roomB, courseA);
    const announcementA = new SwitchAnnouncement(courseA, 'A101 -&gt; TH',
        new Date(), [1, 2]
        , roomA, roomB);
    const announcementB = new ReexamAnnouncement(new Course('Q2'),
        'Nachklausur (A101)', new Date(), [1, 2], roomA);
    const announcementC = new PermanentSwitch(
        new Course('Q2', 'M', 'LK', 1, new Teacher('Td')), 'In G2', new Date(),
        [3, 4], roomA);
    expect(schedule.isAffectedBy(announcementA)).toBeTruthy();
    expect(schedule.isAffectedBy(announcementB)).toBeTruthy();
    expect(schedule.isAffectedBy(announcementC)).toBeFalsy();
  });
});
