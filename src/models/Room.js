/** @constant */
const cgeBuildings = [
  'A',
  'B',
  'C',
  'D',
  'E',
  'G',
  'BIO',
  'KU',
  'PH',
  'CH',
  'KF',
  'TH'];
/** @constant */
const cbgBuildings = ['CBG'];

/** @constant */
const cgeRegExp = '(' + cgeBuildings.join('|') + ')(\\d{1,3})';
/** @constant */
const cbgRegExp = '(' + cbgBuildings.join('|') + ')\\s?(\\d\\.\\d{1,2})';

export default class Room {
  constructor(building, roomNumber) {
    // A109 => A => building, 109 => roomNumber;
    // CBG 1.22; CBG => building, 122 => roomNumber
    if (!(cgeBuildings.concat(cbgBuildings)).includes(
        building)) throw new Error(`Unknown building '${building}'.`);
    this._building = building;
    if (this.building === 'TH') {
      this._roomNumber = null;
      return;
    }
    roomNumber = parseInt(roomNumber);
    if (roomNumber < 0) {
      throw new RangeError(
          '`roomNumber` cannot be negative.');
    }
    this._roomNumber = roomNumber;
  }

  get building() {
    return this._building;
  }

  get roomNumber() {
    return this._roomNumber;
  }

  /**
   * Parses {@link @param str} in order to instantiate a room.
   * @param {string} str - string which contains a room's information.
   * Must contain a sub-string which matches {@link cbgRegExp}
   * or {@link cgeRegExp}.
   * @return {Room|null}
   */
  static parseRoom(str) {
    if (str === 'TH') return new Room('TH', null);
    const combinedRegExp = new RegExp(`(?:${cgeRegExp}|${cbgRegExp})`);
    // console.log(combinedRegExp);
    const result = combinedRegExp.exec(str);
    if (result !== null && result.length === 5) {
      if (result[1] === undefined) {
        // CBG room. Making 1.22 => 122
        return new Room(result[3], parseInt(result[4].replace('.', '')));
      } else {
        // CGE room
        return new Room(result[1], parseInt(result[2]));
      }
    } else {
      return null;
    }
  }

  /**
   * Compares `otherRoom` with the room instance from which this method
   * is called. Equality is given whenever the buildings and the roomNumber
   * are equal.
   * @param {Room} otherRoom
   * @return {boolean}
   */
  isEqualTo(otherRoom) {
    if (!(otherRoom instanceof Room)) {
      throw Error('`otherRoom` must be an instance of Room.');
    }
    return (this.building === otherRoom.building && this.roomNumber ===
        otherRoom.roomNumber);
  }

  toString() {
    return this._building + this._roomNumber.toString();
  }

  /**
   * Converts Room into a JSON-object
   * @return {{roomNumber, building: *}}
   */
  toJSON() {
    return {
      building: this._building,
      roomNumber: this._roomNumber,
    };
  }
};