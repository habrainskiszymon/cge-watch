import Subject from './Subject';
import Teacher from './Teacher';

const courseRegExp = /(Q[1-2])([a-zA-Z]{1,3})(LK|GK|G|L)(\d{1,2})|(Q[1-2])/;

export default class Course {
  /**
   * @constructor
   * @param phase
   * @param type
   * @param number
   * @param subject
   * @param teacher
   */
  constructor(phase, subject, type, number, teacher) {
    if (typeof phase !== 'string' || !['Q1', 'Q2'].includes(phase)) {
      throw new TypeError(
          '`phase` is either not a string or a' +
          ' string representing an unknown phase.');
    }
    this._phase = phase;
    this._representingPhase = false;
    if (type == null) {
      if (number != null || subject != null || teacher != null) {
        throw new Error(
            'When `type` is null the Course instance represents' +
            ' all courses in the specific phase.' +
            'In order to prevent bugs, `number`, `subject`' +
            ' and `teacher` must be null or undefined.' +
            'In this case one or more of those parameters do not fulfill' +
            ' this condition.');
      }
      /*
          Just one example (s. a.):
          Q2-3.-Td-M-Aufsicht Nachklausur (C102)
       */
      this._type = null;
      this._number = null;
      this._subject = null;
      this._teacher = null;
      this._representingPhase = true;
      return;
    }

    if (!['LK', 'L', 'GK', 'G'].includes(type)) {
      throw Error('type is invalid.');
    }

    // normalize
    if (type === 'L') type = 'LK';
    if (type === 'G') type = 'GK';

    if (!Number.isInteger(number) || number < 1) {
      throw new RangeError('`number` must be an integer greater or equal one.');
    }

    if (!Subject.isValidSubjectContraction(subject)) {
      throw Error('`subject` is invalid.');
    }

    if (teacher == null || !(teacher instanceof Teacher)) {
      throw Error('`teacher` must be an instance of Teacher.');
    }

    this._type = type;
    this._number = number;
    this._subject = subject;
    this._teacher = teacher;
  }

  get phase() {
    return this._phase;
  }

  get type() {
    return this._type;
  }

  get number() {
    return this._number;
  }

  get subject() {
    return this._subject;
  }

  /**
   *
   * @return {null|Teacher}
   */
  get teacher() {
    return this._teacher;
  }

  toJSON() {
    if (this.isRepresentingPhase()) {
      return {
        phase: this.phase,
        representingPhase: true,
      };
    } else {
      return {
        phase: this.phase,
        subject: this.subject,
        type: this.type,
        number: this.number,
        teacher: this.teacher ? this.teacher.toJSON() : null,
        representingPhase: false,
      };
    }
  }

  toString() {
    if (this.isRepresentingPhase()) {
      return `Phase: ${this.phase}.`;
    } else {
      return `Course: ${this.phase}${this.level}${Subject.getFullName(
          this.subject)} /w ${this.teacher}`;
    }
  }

  /**
   * Determines whether `anotherCourse` is equal to oneself.
   * @param {Course} anotherCourse
   * @return {boolean}
   */
  isEqualTo(anotherCourse) {
    if (!(anotherCourse instanceof Course)) {
      throw Error('anotherCourse must be an instance of Course.');
    }

    if (this.phase === anotherCourse.phase) {
      if (this.isRepresentingPhase() ||
          anotherCourse.isRepresentingPhase()) return true;

      return (
          this.subject === anotherCourse.subject &&
          this.type === anotherCourse.type &&
          this.number === anotherCourse.number
      );
    } else {
      return false;
    }
  }

  /**
   * Checks whether `interval` is a valid interval or not.
   * @param interval
   * @param maxPeriods
   * return {boolean}
   */
  static isValidPeriodInterval(interval, maxPeriods = 11) {
    return Array.isArray(interval) &&
        (interval.length === 2) &&
        (interval[0] <= interval[1]) &&
        interval.every(
            (period) => {
              return Number.isInteger(period) && period > 0 &&
                  period <= maxPeriods;
            });
  }

  /**
   * Parses the interval represented out of `intervalString`
   * @param {string} intervalString - the string to parse
   * @return {Array.<int>} - the parsed interval string (tuple)
   */
  static parsePeriodInterval(intervalString, maxPeriods) {
    if (maxPeriods == null) maxPeriods = 12;

    if (intervalString.trim().length === 0 || intervalString ==
        null) return null;

    if (typeof intervalString !== 'string') {
      throw new TypeError('Invalid `intervalString` given.');
    }

    let interval = intervalString.replace(/\./g, '').
        // 'x./y.' => 'x/y'
        split('/').
        // 'x/y' => ['x', 'y']
        map((periodString) => {
          // ['x', 'y'] => [x, y]
          // parseInt produces NaN's which we will checkout below
          return parseInt(periodString);
        });

    if (interval[1] === undefined) interval = [interval[0], interval[0]];

    if (interval.some((period) => isNaN(period))) {
      throw new TypeError('Invalid `intervalString` given.');
    }

    if (this.isValidPeriodInterval(interval, maxPeriods)) {
      return interval;
    } else {
      throw new SyntaxError('Invalid `intervalString` given.');
    }
  }

  static parseAndCombineToCourse(str, teacher) {
    if (!(teacher instanceof Teacher)) {
      throw new TypeError('`teacher` must be an instance of Teacher.');
    }
    if (typeof str === 'string') {
      const matches = courseRegExp.exec(str);
      if (matches === null) return null;
      if (matches[1] === undefined) {
        return new Course(matches[0], null, null, null, null);
      } else {
        return new Course(matches[1], matches[2].toUpperCase(), matches[3],
            parseInt(matches[4]), teacher);
      }
    } else {
      throw new TypeError('`str` must be an instance of string.');
    }
  }

  isRepresentingPhase() {
    return this._representingPhase;
  }
}
