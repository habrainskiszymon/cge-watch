import md5 from 'md5';
import Sequelize from 'sequelize';
import loggerFactory from '../logging';

const logger = loggerFactory('models-user');
import getConfiguredSequelize from '../database/configuredSequelize';
// TODO: User 'instanceMethods and classMethods'
const sequelize = getConfiguredSequelize();
// setup User models and its fields.
const User = sequelize.define('users', {
    username: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
        validate: {
            notEmpty: false,
        },
    },
    email: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
        validate: {
            isEmail: true,
        },
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            notEmpty: false,
        },
    },
    subscribed: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
    isAdmin: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
    preferredNotificationHour: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 6,
        validate: {
            min: 0,
            max: 23
        }
    },
    phase: {
        type:   Sequelize.ENUM,
        allowNull: false,
        defaultValue: 'Q2',
        values: ['Q1', 'Q2']
    }
}, {
    hooks: {
        beforeCreate: (user) => {
            return new Promise((res, rej) => {
                user.password = md5(user.password);
                res();
            });
        },
        beforeUpdate: (user) => {
            return new Promise((res, rej) => {
                user.password = md5(user.password);
                res();
            });
        },
        beforeDestroy: (user) => {
            return new Promise((res, rej) => {
                if (user.username === process.env.SUPER_ADMIN_NAME) {
                    rej(new Error('Cannot destroy super-admin.'));
                } else {
                    res();
                }
            });
        },
    },
});

User.prototype.validPassword = function(password) {
    console.log('pswrd check', password, this.password);
    if (typeof password !== 'string') {
        throw new TypeError('`password` must be a string.');
    }
    return md5(password) === this.password;
};

User.prototype.toString = function() {
    return `User[${this.username} /w ${this.email}]`;
};


sequelize.sync()
.then(() => {
    logger.info('`users` table successfully synced.');
    // look if super-admin exists
    return User.count({ where: { username: process.env.SUPER_ADMIN_NAME} })
    .then((count) => {return count !== 0});
})
.then((superAdminExists) => {
    if(superAdminExists) return;
    return User.create({
        username: process.env.SUPER_ADMIN_NAME,
        password: process.env.SUPER_ADMIN_PASSWORD,
        email: process.env.SUPER_ADMIN_EMAIL,
        isAdmin: true,
        subscribed: true
    });
})
.catch(logger.error)

export default User;

/*
then((result) => {
if (result[1] === true) {
logger.log('Created super-admin. ' + result[0].toString() +
' already exists.');
} else {
logger.log('Did not create super-admin. ' + result[0].toString() +
' is already super-admin.');
}})
*/
