const genders = {
  MALE: 0,
  FEMALE: 1,
  UNKNOWN: -1,
};

export default class Teacher {
  /**
   * Represents a teacher in school. Necessary pieces of
   * information are the name, gender and the contraction.
   * @param contraction Required. Necessary to identify the teacher.
   * @param gender 0 => male, 1 => female, -1 => unknown
   * @param name
   */
  constructor(contraction, gender, name) {
    if (typeof contraction !== 'string') {
      throw new TypeError('contraction must be of type string.');
    }
    // TODO: check gender if in range
    if (contraction.length < 2 || contraction.length > 4) {
      throw Error(
          'Contraction must consist of at least 2 to maximum 3 characters.');
    }
    this._name = name ? name : null;
    this._gender = gender != null ? gender : Teacher.GENDER.UNKNOWN;
    this._contraction = contraction.toUpperCase();
  }

  get gender() {
    return this._gender;
  }

  get contraction() {
    return this._contraction;
  }

  get address() {
    if (this.gender === -1) return '';
    return this.gender === 0 ? 'Mr.' : 'Mrs.';
  }

  get name() {
    return this._name;
  }

  static get GENDER() {
    return genders;
  }

  toString() {
    let str = this.gender < 0 ?
        '' :
        (this.address + ' ');
    str += this.name == null ? '' : (this.name + ' ');
    return str + `(${this.contraction})`;
  }

  toJSON() {
    return {
      contraction: this.contraction,
      name: this.name,
      gender: this.gender,
    };
  }
}
// TODO: implement toJSON()
