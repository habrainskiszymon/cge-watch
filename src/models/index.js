/**
 * @module model
 */

export {
  Announcement,
  SwitchAnnouncement,
  ExamAnnouncement,
  ReexamAnnouncement,
  TaskAnnouncement,
  TaskAttendanceAnnouncement,
  CancellationAnnouncement,
  UnknownAnnouncement,
} from './announcement/Announcement';

export Teacher from './Teacher';

export Course from './Course';

export Room from './Room';

export Subject from './Subject';
