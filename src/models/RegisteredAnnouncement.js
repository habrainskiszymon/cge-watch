import md5 from 'md5';
import loggerFactory from '../logging';
import Sequelize from 'Sequelize';
const logger = loggerFactory('models-registered-announcement');
import getConfiguredSequelize from '../database/configuredSequelize';
// TODO: User 'instanceMethods and classMethods'
const sequelize = getConfiguredSequelize();
// setup User models and its fields.
const RegisteredAnnouncement = sequelize.define('registered_announcement', {
  announcement: {
      type: Sequelize.JSON,
      unique: true,
      allowNull: false
  }
});

sequelize.sync()
  .then(() => {
    logger.info('registered_announcement');
  })
  .catch(logger.error)

export default RegisteredAnnouncement;
