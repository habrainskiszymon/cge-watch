// import loggerFactory from '../logging';
// const logger = loggerFactory('models-subject');

const subjectsMap = [
  ['M', 'Mathematics'],
  ['D', 'German'],
  ['E', 'English'],
  ['N', 'Netherlands'],
  ['NF', 'Netherlands Advanced'],
  ['S', 'Spanish'],
  ['F', 'France Beginners'],
  ['FF', 'France Advanced'],
  ['LT', 'Latin'],
  ['KR', 'Catholic Religious Studies'],
  ['ER', 'Protestant Religious Studies'],
  ['SP', 'Physical Education'],
  ['GEA', 'History Beginners'],
  ['GE', 'History Advanced'],
  ['GEB', 'History Bilingual'],
  ['PA', 'Educational Science'],
  ['IF', 'Computer Science'],
  ['BI', 'Biology'],
  ['CH', 'Chemistry'],
  ['PH', 'Physics'],
  ['MU', 'Music'],
  ['LI', 'Literature'],
  ['SWA', 'Social Sciences Beginners'],
  ['SW', 'Social Sciences Advanced'],
  ['EK', 'Geography'],
  ['EKB', 'Geography Bilingual'],
  ['PL', 'Philosophy'],
  ['KU', 'Art'],
];
/**
 * Class Subject. TODO: docs.
 */
export default class Subject {
  /**
   * TODO: docs
   * @param {string} contraction
   * @return {boolean}
   */
  static isValidSubjectContraction(contraction) {
    return subjectsMap.some((subjectTuple) => subjectTuple[0] === contraction);
  }

  /**
   * TODO: docs
   * @param contraction
   * @returns {string[]}
   */
  static getSubject(contraction) {
    if (Subject.isValidSubjectContraction(contraction)) {
      return subjectsMap.find((subjectTuple) => {
        return subjectTuple[0] === contraction;
      });
    }
    return null;
  }

  /**
   * TODO: doc
   * @param contraction
   * @return {string}
   */
  static getFullName(contraction) {
    if(contraction == null) contraction = '';
    const subject = Subject.getSubject(contraction.toUpperCase());
    if (subject == null) {
        return 'No subject specified.';
    }
    return `${subject[1]} (${contraction})`;
  }
}
