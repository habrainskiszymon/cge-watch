import Room from '../Room';
import Course from '../Course';
import _ from 'lodash';

const KEYWORDS = [
  ['Pr&#xE4;senz', 'Präsenz'],
  ['Aufgabe'],
  ['f&#xE4;llt', 'fällt'],
  ['->', '-&gt;'],
  ['Klausur'],
  ['Nachklausur'],
  ['In']
];

/** @todo Separate subclasses by putting them into own directories
 /**
 * Class Announcement.
 */
 export class Announcement {
  /**
   * Announcement's constructor. Must be overridden by subclasses.
   * @constructor
   * @abstract
   * @param {Course} course
   * @param {string} statement
   * @param {Date} date
   * @param {int[]} periodInterval
   */
   constructor(course, statement, date, periodInterval) {
    if (this.constructor === Announcement) {
      throw new Error(
        'Cannot instantiate abstract class Announcement.');
    }
    if (!(course instanceof Course)) {
      throw new TypeError(
        '`course` must be an instance of Course.');
    }
    if (!(date instanceof Date)) {
      throw new TypeError(
        '`date` must be an instance of Date.' +
        ' CGE-date (string) is not allowed.');
    }
    if (typeof statement !== 'string') {
      throw new TypeError('`statement` must be a string and not null.');
    }

    if (!Course.isValidPeriodInterval(periodInterval)) {
      throw new TypeError('`periodInterval` must be a valid period interval.');
    }

    this._course = course;
    this._date = date;
    this._statement = statement;
    this._periodInterval = periodInterval;
  }

  get periodInterval() {
    return this._periodInterval;
  }

  /**
   * Returns course.
   * @return {Course}
   */
   get course() {
    return this._course;
  }

  /**
   * Returns date.
   * @return {Date}
   */
   get date() {
    return this._date;
  }

  get statement() {
    return this._statement;
  }

  /**
   * Returns a string which represents the announcement type.
   */
   get type() {
    return this.constructor.name.toLowerCase().replace('announcement', '');
  }

  /**
   * Converts into JSON.
   * @return {Object}
   * course: {number, teacher, level, subject}}}
   */
   toJSON() {
    return {
      type: this.type,
      statement: this.statement,
      course: this.course.toJSON(),
      date: this.date,
      periodInterval: this.periodInterval,
    };
  }

  /**
   * Builds (and selects) and announcement object. See Factory pattern.
   * @param {Course} course
   * @param {Date} date
   * @param {string} statement - the explanation of an
   * announcement the cge website offers
   * @return {Announcement}
   */
   static createAnnouncement(course, date, statement, periodInterval) {
    if (statement == null || typeof statement !== 'string') {
      throw new TypeError('`statement` must be a string and not null.');
    }

    let keywordIndex = -1;
    for (let i = 0; i < KEYWORDS.length; i++) {
      if (KEYWORDS[i].some((keyword) => {return statement.includes(keyword)})) {
        keywordIndex = i;
        break;
      }
    }

    switch (keywordIndex) {
      case 0:
      return new TaskAttendanceAnnouncement(course, statement, date,
        periodInterval);
      case 1:
      return new TaskAnnouncement(course, statement, date, periodInterval);
      case 2:
      return new CancellationAnnouncement(course, statement, date,
        periodInterval);
      case 3: {
        /** @constant */
        const rooms = SwitchAnnouncement.parseRooms(statement);
        if (rooms == null) {
          throw new TypeError('`statement` does not meet' +
            ' the structure for a switch-announcement');
        }
        return new SwitchAnnouncement(course, statement, date, periodInterval,
          ...rooms);
      }
      case 4:
      return new ExamAnnouncement(course, statement, date, periodInterval);
      case 5:
      return new ReexamAnnouncement(course, statement, date, periodInterval,
        Room.parseRoom(statement));
      case 6: { // preventing eslint-error no-trailing-spaces
        // remove >---+++--+
        //            |||  |
        //            vvv  v
        //           'In A5 ' => 'A5'
        /** @constant */
        const room = Room.parseRoom(statement.replace(/(In|\s+)+/, ''));
        return new PermanentSwitch(course, statement, date, periodInterval,
          room);
      }
      default:
      return new UnknownAnnouncement(course, statement, date, periodInterval);
    }
  }
}

/**
 * Class SwitchAnnouncement.
 */
 export class SwitchAnnouncement extends Announcement {
  /**
   * Class SwitchAnnouncement.
   * @param course
   * @param statement
   * @param date
   * @param periodInterval
   * @param from
   * @param to
   */
   constructor(course, statement, date, periodInterval, from, to) {
    super(course, statement, date, periodInterval);
    if (!(from instanceof Room && to instanceof Room) || from == null || to ==
      null) {
      throw new Error('`from` and `to` must be instances of Room.');
  }
  if (from.isEqualTo(to)) {
    throw Error(
      'Not real movement given.');
  }

  this._from = from;
  this._to = to;
}

  /**
   * Returns the room from which the course will move.
   * @return {Room}
   */
   get from() {
    return this._from;
  }

  /**
   * Returns the room to which the course will move.
   * @return {Room}
   */
   get to() {
    return this._to;
  }

  /**
   * Parses the rooms out of the statement.
   * @param {string} statement - Statement given by the CGE website in order to
   * specify from which room (X) and to which room (Y) the course will move.
   * Syntax: 'X -> Y' displayed by HTML-Entities.
   * @return {Room[]}
   */
   static parseRooms(statement) {
    if (typeof statement === 'string') {
      const regExp = new RegExp(
        '^((?!\\s).+(?<!\\s))\\s?(?:-&gt;|->)\\s?((?!\\s).+(?<!\\s))$');
      const matches = regExp.exec(statement);
      if (matches !== null && matches.length === 3) {
        const roomA = Room.parseRoom(matches[1]);
        const roomB = Room.parseRoom(matches[2]);
        if (roomA === null || roomB === null) {
          return null;
        }
        return [roomA, roomB];
      }
      return null;
    }
  }

  /**
   * @see Announcement.toJSON
   */
   toJSON() {
    return Object.assign(super.toJSON(), {
      from: this._from,
      to: this._to,
    });
  }
}

/**
 * Class CancellationAnnouncement.
 */
 export class CancellationAnnouncement extends Announcement {
 }

/**
 * Class ExamAnnouncement.
 */
 export class ExamAnnouncement extends Announcement {
 }

/**
 * Class TaskAnnouncement.
 */
 export class TaskAnnouncement extends Announcement {
 }

/**
 * Class TaskAttendanceAnnouncement.
 */
 export class TaskAttendanceAnnouncement extends Announcement {
 }

/**
 * Class PermanentSwitch.
 */
 export class PermanentSwitch extends Announcement {
  /**
   * @constructor
   * @param {Course} course
   * @param {string} statement
   * @param {Date} date
   * @param {int[]}periodInterval
   * @param {Room} to
   */
   constructor(course, statement, date, periodInterval, to) {
    super(course, statement, date, periodInterval);

    if (to == null || !(to instanceof Room)) {
      throw new Error('`to` must be an instance of Room.');
    }

    this._to = to;
  }

  /**
   * Returns the room in which the course will permanently switch to.
   * @return {Room}
   */
   get to() {
    return this._to;
  }

  /**
   * @see Announcement.toJSON
   * @return {Object}
   */
   toJSON() {
    return Object.assign(super.toJSON(), {
      to: this.to,
    });
  }
}

export class ReexamAnnouncement extends Announcement {
  /**
   * @constructor
   * @see Announcement#constructor
   * @param {Course} course
   * @param {string} statement
   * @param {Date} date
   * @param {int[]} periodInterval
   * @param {Room} room
   */
   constructor(course, statement, date, periodInterval, room) {
    super(course, statement, date, periodInterval);
    if (room == null || !(room instanceof Room)) {
      throw new TypeError('`room` must be an instance of Room and not null.');
    }
    this._in = room;
  }

  /**
   * Returns in which room the reexamination will take place.
   * @return {Room}
   */
   get in() {
    return this._in;
  }

  /**
   * Converts itself into a JSON-object.
   * @return {{date, periodInterval, statement, course, type} & {in: Room}}
   */
   toJSON() {
    return Object.assign(super.toJSON(), {
      in: this.in,
    });
  }
}

/**
 * Class UnknownAnnouncement.
 */
 export class UnknownAnnouncement extends Announcement {
 }
