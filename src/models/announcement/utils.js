import cheerio from 'cheerio';
const _ = require('lodash');
import {Announcement, Course, Teacher} from './../../models';
import loggerFactory from './../../logging';
const logger = loggerFactory('model-announcement-utils');

const TABLE_IDENTIFIER = '.schuelervplan';

export function convertISODateIntoCgeDate(date) {
    if(isNaN(date)) {
        throw new TypeError('Invalid `date` given.');
    }

    if (date == null || !(date instanceof Date)) {
        throw new TypeError('`date` must be an instance of native type Date.');
    }

    return _.padStart(date.getDate(), 2, '0') + '.'
    + _.padStart((date.getMonth() + 1), 2, '0') + '.'
    + date.getFullYear();
}

export function convertCgeDateIntoISODate(date) {
    if(date.length !== 10) {
        throw new TypeError('`date` must be string of length 10 considering the format.');
    }

    if (typeof date === 'string') {
        const dateFragments = date.split('.');
        if (dateFragments.length === 3) {
            return new Date(Date.parse(dateFragments.reverse().join('-')));
        }

        throw new TypeError('`date` must be a string' +
        ' and in the format DD.MM.YYYY.');
    }
}

export function parseAnnouncementDates(html) {
    const $ = cheerio.load(html);
    const dates = [];
    $('select[name=schdatumauswahl]').children('option').each((i, option) => {
        // convert from DD.MM.YYYY into YYYY-MM-DD (ISO 8601)
        dates.push(convertCgeDateIntoISODate($(option).html()));
    });
    return dates;
}

/**
* Parses and builds {@link Announcement} objects out of `html`.
* @param {string} html
* @param {Date} date
* @return {Announcement[]}
*/
export function parseAnnouncements(html, date) {
    if (typeof html !== 'string' || html == null) {
        throw new TypeError('`html` must be a string and not null.');
    }
    if (html.length === 0) return [];
    if (!(date instanceof Date) || date == null) {
        throw new TypeError('`date` must be an instance of Date and not null.');
    }

    const $ = cheerio.load(html);
    // chunk the array and convert everything into strings
    let lastInterval = [1, 1];
    return _.chunk(
        $(`${TABLE_IDENTIFIER} > tbody > tr > td`).slice(7, -1), 6).
        map((cells) => {
            cells = cells.map((cell) => $(cell).text());
            try {
                cells[0] = cells[0].replace(/ /g, '');
                const teacher = new Teacher(cells[2] ?
                    cells[2] :
                    cells[5] ? cells[5] : null, null, null);
                    if(teacher == null) {
                        logger.error(
                            'No teacher was found in cells:',
                            {cells: cells}
                        );
                    }
                    const course = Course.parseAndCombineToCourse(cells[0], teacher);
                    let periodInterval = Course.parsePeriodInterval(cells[1]);
                    if (periodInterval == null) {
                        periodInterval = lastInterval;
                    } else {
                        lastInterval = periodInterval;
                    }
                    const announcement = Announcement.createAnnouncement(course, date,
                        cells[4], periodInterval);
                        return announcement;
                    } catch (e) {
                        logger.error({
                            message: new Error('Cannot fetch announcement.'),
                            errorOccurred: JSON.stringify(e, Object.getOwnPropertyNames(e)),
                            announcementInfo: cells
                        });

                        throw e;
                    }
                });
            }

export function groupAnnouncementsByDate(announcements) {
    // sorting rule: => date => period
    announcements.sort((a, b) => {
        if(a.date < b.date) {
            return -1;
        } else if (a.date === b.date) {
            if(a.periodInterval[0] < b.periodInterval[0]) {
                return -1;
            } else if (a.periodInterval[0] > b.periodInterval[0]) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 1;
        }
    });


    // date1 : [a1, a2, a3, ..., aP], date2: [a1, a2, a3, ..., aQ], ..., dateN : [a1, a2, a3, ..., aR]]
    const groupedAnnouncements = {};
    for (let announcement of announcements) {
        if(groupedAnnouncements[announcement.date] == null) {
            groupedAnnouncements[announcement.date] = [];
        }
        groupedAnnouncements[announcement.date].push(announcement);
    }

    return groupedAnnouncements;
}
