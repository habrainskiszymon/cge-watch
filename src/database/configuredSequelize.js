import sequelize from 'sequelize';
import loggerFactory from './../logging';
import path from 'path';

const logger = loggerFactory('db', false, false);

export default function getConfiguredSequelize() {
  return new sequelize.Sequelize({
    storage: path.join(__dirname, 'database.db'),
    dialect: 'sqlite',
    logging: logger.info.bind(logger)
  });
}
